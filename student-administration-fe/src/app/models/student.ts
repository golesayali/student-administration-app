export class Student {
  id: number;
  name: string;
  birthDate: number;
  address: string;
  createdBy: String;
  createdDate: String;
  lastModifiedBy: String;
  lastModifiedDate: String;
}
